

var towns = [
    'Kolomiya',
    'Otynia',
    'Horodenka',
    'Sniatyn',
    'Palianucya',
    'Yasina',
    'Nadvirna',
    'Deliatyn'
];

var locations = {
    getDistanceBetweenUserAndAnyTown: function (town, callback) {
        locations.getLocationByQuery(town, function (queryLocationResponse) {
            locations.getLocationOfUser(function (userLocationResponse) {
                callback(town, locations.getDistanceBetweenTwoPoints(
                    userLocationResponse.lat,
                    userLocationResponse.lng,
                    queryLocationResponse.lat,
                    queryLocationResponse.lng
                ));
            });
        });
    },

    getLocationByQuery: function (query, callback) {
        var callAgain = false, amountOfRepetitionCalls = 0;

        do {
            $.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + query, function (response) {
                if (response && response.results && response.results[0] && response.results[0].geometry) {
                    amountOfRepetitionCalls = 0;
                    callback(response.results[0].geometry.location);
                } else {
                    amountOfRepetitionCalls++;
                    if (amountOfRepetitionCalls > 100) return;
                    callAgain = true;
                }
            });
        } while (callAgain);
    },

    getLocationOfUser: function (callback) {
        navigator.geolocation.getCurrentPosition(function (location) {
            callback({
                lat: location.coords.latitude,
                lng: location.coords.longitude,
            });
        });
    },

    getDistanceBetweenTwoPoints: function (lat1, lon1, lat2, lon2) {
        var earthRadius = 6371;
        var distanceLat = this._toRad(lat2 - lat1);
        var distancelng = this._toRad(lon2 - lon1);
        lat1 = this._toRad(lat1);
        lat2 = this._toRad(lat2);

        var a = Math.sin(distanceLat / 2) * Math.sin(distanceLat / 2) +
            Math.sin(distancelng / 2) * Math.sin(distancelng / 2) * Math.cos(lat1) * Math.cos(lat2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadius * c;
    },

    /**
     * Converts numeric degrees to radians
     *
     */
    _toRad: function (Value) {
        return Value * Math.PI / 180;
    }
};

var elements = {
    page: {
        id: 'page',

        getElements: function () {
            return $('#' + this.id);
        },

        onLoadExecute: function () {
            elements.townsList.items = towns;
            elements.townsList.render();
        },

        show: function () {
            this.getElements().css('display', 'block');
        },

        hide: function () {
            this.getElements().css('display', 'none');
        }
    },
    townsList: {
        id: 'towns-list',
        items: [],

        /**
         * Contains list of objects { town: {string}, distance: {number} }
         *
         */
        distanceMap: [],

        getElement: function () {
            return $('#' + this.id);
        },

        clear() {
            this.getElement().html('');
        },

        render: function () {
            this.clear();

            for (var i = 0; i < this.items.length; i++) {
                locations.getDistanceBetweenUserAndAnyTown(elements.townsList.items[i], function (town, distance) {
                    elements.townsList.distanceMap.push({town: town, distance: distance});
                    var itemText = town + ' - ' + '<span class="badge badge-secondary">' + distance.toFixed(3) + ' km </span>';

                    elements.townsList.getElement().append('<li class="list-group-item">' + itemText + '</li>');
                    elements.townsList.theClosestTown.render();
                    elements.page.show();
                });
            }
        },

        addItem: function (element) {
            this.items.push(element);
            this.render();
        },

        theClosestTown: {
            id: 'the-closest-town',

            getElement: function () {
                return $('#' + this.id);
            },

            render: function () {
                var theClosestTown = this._getTheClosestTown();

                this.clear();
                this.getElement().append(theClosestTown.town + ' - ' +
                    '<span class="badge badge-secondary">' + theClosestTown.distance.toFixed(3) + ' km </span>'
                    + ' The closest');
            },

            clear() {
                this.getElement().html('');
            },

            _getTheClosestTown: function () {
                var theClosestTown = elements.townsList.distanceMap[0];

                for (var i = 0; i < elements.townsList.distanceMap.length; i++) {
                    if (theClosestTown.distance > elements.townsList.distanceMap[i].distance)
                        theClosestTown = elements.townsList.distanceMap[i];
                }

                return theClosestTown;
            }
        }
    }
};

elements.page.onLoadExecute();
